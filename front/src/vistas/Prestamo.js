import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import MaterialDatatable from "material-datatable";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2)

  },
  delete: {
    backgroundColor: "red"
  }

}));

export default function Prestamo() {
  const classes = useStyles();
  const fechaActual = new Date();
  const fechaGuardada = (fechaActual.getFullYear() + '-' +(fechaActual.getMonth()+1) + '-' + fechaActual.getDate());
  const { register, handleSubmit, reset } = useForm(
    { defaultValues: { fecha: fechaActual.getFullYear() + '-' +(fechaActual.getMonth()+1) + '-' + fechaActual.getDate() } });

  const [] = useState(0)
  const [accion] = useState("Guardar")
  const [] = useState(null);
  const [personaSeleccionado, setPersonaSeleccionado] = useState(0);
  const [libroSeleccionado, setLibroSeleccionado] = useState(0);
  const [libros, setLibros] = useState(null)
  const [personas, setPersonas] = useState(null)
  const [prestamos, setPrestamos] = useState([])

  useEffect(() => {
    cargarPersona();
    cargarLibro();
    cargarPrestamo();
  }, []);

  const columns = [
    {
      name: 'Nombre Persona',
      field: 'personas'
    },
    {
      name: 'Nombre Libro',
      field: 'libros'
    },
    {
      name: 'Fecha Prestamo',
      field: 'fecha'
    }
  ];


  const options = {
    selectableRows: false,
    print: false,
    onlyOneRowCanBeSelected: false,
    textLabels: {
      body: {
        noMatch: "Lo sentimos, no se encuentran registros",
        toolTip: "Sort",
      },
      pagination: {
        next: "Siguiente",
        previous: "Página Anterior",
        rowsPerPage: "Filas por página:",
        displayRows: "de",
      },
    },
    download: false,
    pagination: true,
    rowsPerPage: 5,
    usePaperPlaceholder: true,
    rowsPerPageOptions: [5, 10, 25],
    sortColumnDirection: "desc",
  }
  const onSubmit = data => {
    console.log(data)
    if (accion == "Guardar") {
      axios
        .post("http://localhost:9000/api/prestamo",
          {
            idPersona: personaSeleccionado,
            libro: libroSeleccionado,
            fecha: fechaGuardada
          })
        .then(
          (response) => {
            if (response.status == 200) {
              alert("Registro de prestamo realizado exitosamente")
              console.log(data);
              cargarPrestamo();
              reset();
            }
          },
          () => {
            // Swal.fire(
            //   "Error",
            //   "No es posible realizar esta acción: " + error.message,
            //   "error"
            // );
          }
        )
        .catch((error) => {
          // Swal.fire(
          //   "Error",
          //   "No cuenta con los permisos suficientes para realizar esta acción",
          //   "error"
          // );
          console.log(error);
        });
    }

  }



  const cargarPersona = async () => {
    // const { data } = await axios.get('/api/zona/listar');

    const { data } = await axios.get("http://localhost:9000/api/personas");

    setPersonas(data.persona);
    console.log(data);


  };
  const cargarLibro = async () => {
    // const { data } = await axios.get('/api/zona/listar');

    const { data } = await axios.get("http://localhost:9000/api/libro");

    setLibros(data.libroConAutor);


  };

  const cargarPrestamo = async () => {
    // const { data } = await axios.get('/api/zona/listar');

    const { data } = await axios.get("http://localhost:9000/api/prestamo");

    setPrestamos(data.resultado);


  };

  const ModificaPersonaSeleccionada = (event) => {
    setPersonaSeleccionado(event.target.value);
  };
  
  const ModificaLibroSeleccionado = (event) => {
    setLibroSeleccionado(event.target.value);
  };

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Registro de Prestamo
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Select
                labelId="demo-customized-select-label"
                id="demo-customized-select"
                style={{ width: "100%" }}
                onChange={ModificaPersonaSeleccionada}
                value={personaSeleccionado}
                labelWidth={"Autor"}
                margin="dense"
                placeholder={"Horarios"}
              >
                <MenuItem selected={true} key={1} value={0}>
                  Seleccione Persona para agendar el prestamo
                </MenuItem>
                {personas !== null ? (
                  personas.map((item, index) => {
                    return (
                      <MenuItem key={index} value={item._id}>
                        <em>{item.nombre}</em>
                      </MenuItem>
                    );
                  })
                ) : (
                    <MenuItem key={-1} value={0}>
                      <em>''</em>
                    </MenuItem>
                  )}
              </Select>
            </Grid>
            <Grid item xs={12}>
              <Select
                labelId="demo-customized-select-label"
                id="demo-customized-select"
                style={{ width: "100%" }}
                onChange={ModificaLibroSeleccionado}
                value={libroSeleccionado}
                labelWidth={"Autor"}
                margin="dense"
                placeholder={"Horarios"}
              >
                <MenuItem selected={true} key={1} value={0}>
                  Seleccione Libro a prestar
                </MenuItem>
                {libros !== null ? (
                  libros.map((item, index) => {
                    return (
                      <MenuItem key={index} value={item._id}>
                        <em>{item.nombre}</em>
                      </MenuItem>
                    );
                  })
                ) : (
                    <MenuItem key={-1} value={0}>
                      <em>''</em>
                    </MenuItem>
                  )}
              </Select>
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="fecha"
                label="fecha"
                id="fecha"
                placeholder="Fecha actual"
                disabled = "true"
                inputRef={register}
        
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {accion}
          </Button>
          {/* <Grid container spacing={1}>
            <MaterialDatatable

              title={"Prestamos"}
              data={prestamos}
              columns={columns}
              options={options}
            />
          </Grid>  */}


        </form>


      </div>

    </Container>
  );
}